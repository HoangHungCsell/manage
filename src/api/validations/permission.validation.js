const Joi = require('joi');

module.exports = {
    createValidation: {
        body: {
            role: Joi.string()
                .required(),
            permission: Joi.array()
                .items(Joi.string()
                    .default('read')
                )

        }
    }
}