const Joi = require('joi');

module.exports = {
    listValidation: {
        query: {
            skip: Joi.number()
                .min(0)
                .default(0),
            limit: Joi.number()
                .default(10),
            image: Joi.array()
                .items(Joi.string())
                .allow(null, ''),
            keyword: Joi.string()
                .allow(null, ''),
            min_price: Joi.number()
                .allow(null, ''),
            max_price: Joi.number()
                .allow(null, ''),
            date_by: Joi.string()
                .only(['create', 'update'])
                .default('create')
                .allow(null, ''),
            start_time: Joi.date()
                .allow(null, ''),
            end_time: Joi.date()
                .allow(null, ''),
            sort_by: Joi.string()
                .only(['name', 'price', 'create', 'update'])
                .allow(null, '')
                .default('name'),
            order_by: Joi.string()
                .only(['asc', 'desc'])
                .allow(null, '')
                .default('asc')
        }
    },
    createValidation: {
        body: {
            image: Joi.string()
                .required(),
            name: Joi.string()
                .required(),
            description: Joi.string()
                .max(255)
                .default(''),
            price: Joi.number()
                .required(),
            discount: Joi.string()
        }
    },
    updateValidation: {
        body: {
            image: Joi.string()
                .allow(null, ''),
            name: Joi.string()
                .allow(null, ''),
            description: Joi.string()
                .max(255)
                .default(''),
            price: Joi.number()
                .allow(null, ''),
            discount: Joi.string()
                .allow(null, '')
        }
    }
};