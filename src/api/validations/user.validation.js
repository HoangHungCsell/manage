const Joi = require('joi');
const { values } = require('lodash');
const User = require('../models/user.model');

module.exports = {
    listValidation: {
        query: {
            keyword: Joi.string()
                .allow(null, ''),
            address: Joi.string()
                .allow(null, ''),
            genders: Joi.array()
                .items(Joi.string())
                .allow(null, ''),
            roles: Joi.array()
                .items(Joi.string())
                .allow(null, ''),
            skip: Joi.number()
                .min(0)
                .default(0),
            limit: Joi.number()
                .default(10)
        }
    },
    createValidation: {
        body: {
            name: Joi.string()
                .required(),
            username: Joi.string()
                .required(),
            password: Joi.string()
                .min(6)
                .max(15)
                .required(),
            address: Joi.string()
                .required(),
            phone: Joi.string()
                .length(10)
                .required(),
            gender: Joi.string()
                .only(values(User.Genders))
                .required(),
            roles: Joi.array()
                .items(Joi.string()
                    .only(values(User.Roles))
                )
                .default('read')
        }
    },
    updateValidation: {
        body: {
            name: Joi.string()
                .allow(null, ''),
            username: Joi.array()
                .unique()
                .allow(null, ''),
            password: Joi.string()
                .min(6)
                .max(15)
                .allow(null, ''),
            address: Joi.string()
                .allow(null, ''),
            phone: Joi.string()
                .length(10)
                .allow(null, ''),
            gender: Joi.string()
                .only(values(User.Genders))
                .allow(null, ''),
            roles: Joi.array()
                .items(Joi.string()
                    .only(values(User.Roles))
                )
                .allow(null, '')
        }
    }
};