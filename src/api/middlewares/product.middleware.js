const Product = require('../models/product.model');

exports.condition = async (req, res, next) => {
    try {
        const params = req.query ? req.query : {};
        let start; let end;
        if (params.start_time && params.end_time) {
            start = new Date(params.start_time); end = new Date(params.end_time);
            start.setHours(0, 0, 0, 0); end.setHours(23, 59, 59, 999);
        }


        const conditions = [
            {
                image: params.image
                    ? { $in: params.image }
                    : { $exists: true }
            },
            {
                name: params.keyword ? new RegExp(params.keyword, 'i') : { $exists: true }
            },
            {
                price: params.min_price
                    ? { $gte: params.min_price }
                    : { $exists: true }
            },
            {
                price: params.max_price
                    ? { $lte: params.max_price }
                    : { $exists: true }
            },
            {
                updated_at: params.date_by === 'update' && start && end
                    ? { $gte: start, $lte: end }
                    : { $exists: true }
            },
            {
                created_at: params.date_by === 'create' && start && end
                    ? { $gte: start, $lte: end }
                    : { $exists: true }
            }

        ];
        req.conditions = conditions;
        return next();
    } catch (error) {
        return (error);
    }
};

exports.load = async (req, res, next) => {
    try {
        const product = await Product.getProductById(req.params.productId);

        req.locals = req.locals ? req.locals : {};
        req.locals.product = product;

        return next();
    } catch (error) {
        return (error);
    }
};

exports.count = async (req, res, next) => {
    try {
        req.totalDocuments = await Product.find({ $and: req.conditions }).countDocuments();
        return next();
    } catch (error) {
        return (error);
    }
};

exports.sort = (req, res, next) => {
    const params = req.query ? req.query : {};
    const sort = params.order_by === 'desc' ? -1 : 1;
    switch (params.sort_by) {
        case 'name':
            req.sorts = { name: sort };
            break;
        case 'price':
            req.sorts = { price: sort };
            break;
        case 'create':
            req.sorts = { created_at: sort };
            break;
        case 'update':
            req.sorts = { updated_at: sort };
            break;
        default:
            req.sorts = { name: sort };
            break;
    }
    next();
};

