const User = require('../models/user.model');

exports.condition = async (req, res, next) => {
    try {
        const params = req.query ? req.query : {};

        const conditions = [
            {
                $or: [
                    {
                        username: params.keyword ? new RegExp(params.keyword, 'i') : { $exists: true }
                    },
                    {
                        name: params.keyword ? new RegExp(params.keyword, 'i') : { $exists: true }
                    }
                ]
            },
            {
                address: params.address ? new RegExp(params.keyword, 'i') : { $exists: true }
            },
            {
                gender: params.genders
                    ? { $in: params.genders }
                    : { $exists: true }
            },
            {
                roles: params.roles
                    ? { $in: params.roles }
                    : { $exists: true }
            }
        ];

        req.conditions = conditions;
        return next();
    } catch (error) {
        return (error);
    }
};

exports.load = async (req, res, next) => {
    try {
        const user = await User.getUserById(req.params.userId);
        req.locals = req.locals ? req.locals : {};
        req.locals.user = user;
        return next();
    } catch (error) {
        return error;
    }
};