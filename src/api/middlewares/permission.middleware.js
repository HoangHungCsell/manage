
const User = require('../models/user.model');
function authorize(roles) {
    return [
        (req, res, next) => {
            User.findById(req.userData.id, function (error, user) {
                if (error)
                    return (error);
                if (!user) return res.json({ message: 'Tài khoản không tồn tại!' });
                const check = user.roles.find(x => x === roles);

                if (!check) return res.status(401).json({ message: 'Bạn không có quyền truy cập hoặc thực hiện thao tác này!' });
                next();
            });

        }
    ];
}

module.exports = authorize;