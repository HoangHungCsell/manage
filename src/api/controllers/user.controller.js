const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcryptjs = require('bcryptjs');
const User = require('../models/user.model');


exports.list_all_users = async (req, res) => {
    try {
        const users = await User.aggregate([
            {
                $match: {
                    $and: req.conditions
                }
            },
            {
                $skip: req.query.skip
            },
            {
                $limit: req.query.limit
            }
        ]);

        return res.json({
            user: users.map(user => new User(user).transform())
        });
    } catch (error) {
        return (error);
    }
};

exports.read_a_user = async (req, res) => {
    try {
        return res.json(req.locals.user.transform());
    } catch (error) {
        return error;
    }
};

exports.update_a_user = async (req, res) => {
    try {
        req.body.updated_at = new Date();

        const user = Object.assign(req.locals.user, req.body);
        await user.save();

        return res.json(user.transform());
    } catch (error) {
        return (error);
    }
};

exports.delete_a_user = async (req, res) => {
    try {
        req.locals.user.remove();
        return res.json({
            message: 'Đã xóa user thành công!',
            user: req.locals.user.transform()
        })
    } catch (error) {
        return error;
    }
};

exports.register = (req, res) => {
    const { username, password, name, address, phone, gender, roles } = req.body;
    bcryptjs.hash(password, 10, (err, hash) => {
        if (err)
            res.status(400).send(err);
        else {
            const newUser = new User({
                _id: mongoose.Types.ObjectId(),
                username,
                password,
                name,
                address,
                phone,
                gender,
                roles,
                hash_password: hash
            });
            newUser.save((err, user) => {
                if (err)
                    res.status(400).send(err);
                else {
                    user.password = undefined;
                    user.hash_password = undefined;
                    return res.json(user.transform());
                }
            });
        }
    });
};

exports.login = async (req, res) => {
    await User.findOne(
        {
            username: req.body.username
        }, (err, user) => {
            if (err) throw err;
            if (!user) {
                res.json({ message: 'Tài khoản không tồn tại!' });
            }
            if (user.password !== req.body.password) {
                res.json({ message: 'Mật khẩu không đúng!' });
            } else {
                res.json({ token: jwt.sign({ id: user._id, username: user.username, roles: user.roles }, 'HoangHung', { expiresIn: '1d' }) });
            }
        }
    );
};


exports.loginRequired = function (req, res, next) {
    if (req.user) {
        next();
    } else {
        return res.status(401).json({ message: 'Unauthorized user!' });
    }
};