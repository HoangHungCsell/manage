const mongoose = require('mongoose');
const { pick } = require('lodash');
const Product = require('../models/product.model');

exports.create = async (req, res) => {
    try {
        req.body.created_by = {
            id: req.userData.id,
            username: req.userData.username
        }
        const product = new Product(req.body);
        await product.save();
        return res.json(product.transform());
    } catch (error) {
        return (error);
    }
};

exports.list = async (req, res) => {
    try {
        const products = await Product.aggregate([
            {
                $match: {
                    $and: req.conditions
                }
            },
            {
                $sort: req.sorts
            },
            {
                $skip: req.query.skip
            },
            {
                $limit: req.query.limit
            }
        ]);

        return res.json({
            count: req.totalDocuments,
            product: products.map(product => new Product(product).transform())
        });
    } catch (error) {
        return (error);
    }
};

exports.detail = async (req, res) => {
    res.json(req.locals.product.transform());
};

exports.update = async (req, res) => {
    try {

        req.body.updated_at = new Date();

        const product = Object.assign(req.locals.product, req.body);
        await product.save();

        return res.json(product.transform());
    } catch (error) {
        return (error);
    }
};

exports.delete = async (req, res) => {
    try {
        req.locals.product.remove();
        res.json({
            message: 'Đã xóa sản phẩm thành công!',
            product: req.locals.product.transform()
        })
    } catch (error) {
        return (error);
    }
};