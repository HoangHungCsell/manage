const mongoose = require('mongoose');
const Permission = require('../models/permission.model');

exports.permission = async (req, res) => {
    const { role, permission } = req.body;
    const newPermission = new Permission({
        _id: mongoose.Types.ObjectId(),
        role,
        permission
    });
    await newPermission.save((err, permission) => {
        if (err)
            res.send(err);
        res.json(permission);
    });
};