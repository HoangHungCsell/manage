module.exports = app => {
    const validate = require('express-validation');
    const controller = require('../controllers/product.controller');
    const auth = require('../middlewares/login.middleware');
    const middleware = require('../middlewares/product.middleware');
    const permission = require('../middlewares/permission.middleware');
    const {
        listValidation,
        createValidation,
        updateValidation
    } = require('../validations/product.validation');

    app.route('/products')
        .get(
            auth,
            permission('list'),
            validate(listValidation),
            middleware.condition,
            middleware.count,
            middleware.sort,
            controller.list
        )
        .post(
            auth,
            permission('create'),
            validate(createValidation),
            controller.create
        );
    app.route('/products/:productId')
        .get(
            auth,
            permission('read'),
            middleware.load,
            controller.detail
        )
        .put(
            auth,
            permission('update'),
            validate(updateValidation),
            middleware.load,
            controller.update)
        .delete(
            auth,
            permission('delete'),
            middleware.load,
            controller.delete
        );
};