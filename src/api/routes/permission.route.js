module.exports = app => {
    const validate = require('express-validation');
    const controller = require('../controllers/permission.controller');
    const { createValidation } = require('../validations/permission.validation');

    app.route('/permission')
        .post(validate(createValidation), controller.permission);
};