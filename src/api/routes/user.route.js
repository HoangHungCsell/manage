
module.exports = app => {
    const validate = require('express-validation');
    const controller = require('../controllers/user.controller');
    const auth = require('../middlewares/login.middleware');
    const middleware = require('../middlewares/user.middleware');
    const permission = require('../middlewares/permission.middleware');
    const {
        listValidation,
        createValidation,
        updateValidation
    } = require('../validations/user.validation');


    app.route('/users')
        .get(
            auth,
            permission('list'),
            validate(listValidation),
            middleware.condition,
            controller.list_all_users
        );

    app.route('/users/:userId')
        .get(
            auth,
            permission('read'),
            middleware.load,
            controller.read_a_user
        )
        .put(
            auth,
            permission('update'),
            validate(updateValidation),
            middleware.load,
            controller.update_a_user
        )
        .delete(
            auth,
            permission('delete'),
            middleware.load,
            controller.delete_a_user
        );

    app.route('/register')
        .post(
            validate(createValidation),
            // permission('create'),
            controller.register
        );

    app.route('/login')
        .post(controller.login);
};