const mongoose = require('mongoose');

const permissionSchema = new mongoose.Schema(
    {
        _id: {
            type: String,
            required: true
        },
        role: {
            type: String,
            required: true
        },
        permission: {
            type: [String],
            enum: ["create", "update", "delete", "list", "read"],
            default: 'read'
        }

    },
    {
        versionKey: false
    }
);

module.exports = mongoose.model('Permission', permissionSchema, 'permissions');