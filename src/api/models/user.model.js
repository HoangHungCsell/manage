const mongoose = require('mongoose');
const { isEqual } = require('lodash');
const bcryptjs = require('bcryptjs');

const Genders = {
    MALE: 'male',
    FEMALE: 'female',
    UNKNOWN: 'unknown'
};

const Roles = {
    CREATE: 'create',
    READ: 'read',
    UPDATE: 'update',
    DELETE: 'delete',
    LIST: 'list',
    UNKNOWN: 'unknown'
};

const userSchema = new mongoose.Schema(
    {
        _id: {
            type: String,
            required: true
        },
        name: {
            type: String,
            required: true
        },
        username: {
            type: String,
            unique: true,
            required: true
        },
        password: {
            type: String,
            required: true
        },
        hash_password: String,
        address: {
            type: String,
            required: true
        },
        phone: {
            type: String,
            unique: true,
            required: true
        },
        gender: {
            type: String,
            required: true
        },
        roles: {
            type: [String],
            default: 'read'
        },
        created_at: {
            type: Date,
            default: () => new Date()
        },
        updated_at: {
            type: Date,
            default: () => new Date()
        }
    },
    {
        timestamps: false,
        versionKey: false
    }
);

userSchema.methods.comparePassword = password => {
    // return bcryptjs.compareSync(password, this.hash_password);
    return bcryptjs.compare(password, this.hash_password, (err, rusult) => {
        if (err) return res.status(404).send({ message: err });
        else {
            let token = jwt.sign({
                username: user.username,
                name: user.name,
                _id: user._id
            },
                'HoangHung',
                {
                    expiresIn: "1d"
                }
            );
        }
    });
};

userSchema.method({
    transform() {
        const transformed = {};
        const fields = [
            'id',
            'name',
            'username',
            'address',
            'phone',
            'gender',
            'roles',
            'created_at',
            'updated_at'
        ];

        fields.forEach(field => {
            transformed[field] = this[field];
        });
        return transformed;
    }
});

userSchema.static({
    Genders,
    Roles,
    async getUserById(id) {
        try {
            const user = await this.findById(id);
            if (!user)
                return 'Không tìm thấy dữ liệu!';
            return user;
        } catch (error) {
            return error;
        }
    }
});

module.exports = mongoose.model('User', userSchema, 'users');