const mongoose = require('mongoose');
const { mongo } = require('mongoose');

const productSchema = new mongoose.Schema(
    {
        _id: {
            type: String,
            required: false
        },
        image: {
            type: String,
            required: true
        },
        name: {
            type: String,
            required: true
        },
        description: {
            type: String,
            maxlength: 255,
            default: ''
        },
        price: {
            type: Number,
            required: true
        },
        discount: {
            type: String
        },
        created_by: {
            id: {
                type: String,
                required: true
            },
            username: {
                type: String,
                required: true
            }
        },
        created_at: {
            type: Date,
            default: () => new Date()
        },
        updated_at: {
            type: Date,
            default: () => new Date()
        }
    },
    {
        versionKey: false
    }
);

productSchema.pre('save', async function save(next) {
    try {
        this.wasNew = this.isNew;
        if (this.wasNew) {
            this._id = new mongo.ObjectId().toHexString();
        }
        return next();
    } catch (error) {
        return (error);
    }
});

productSchema.method({
    transform() {
        const transformed = {};
        const fields = [
            '_id',
            'image',
            'name',
            'description',
            'price',
            'discount',
            'created_by',
            'created_at',
            'updated_at'
        ];

        fields.forEach(field => {
            transformed[field] = this[field];
        });
        return transformed;
    }
});

productSchema.static({

    async getProductById(id) {
        try {
            const product = this.findById(id);

            if (!product)
                return 'Không tìm thấy dữ liệu';
            return product;

        } catch (error) {
            return (error);
        }
    }
});

module.exports = mongoose.model('Product', productSchema, 'products');