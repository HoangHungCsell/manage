require('dotenv').config();

const express = require('express'),
    app = express(),
    port = process.env.PORT,
    mongoose = require('mongoose'),
    bodyParser = require('body-parser');

mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGO_URL);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    if (req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'JWT') {
        jwt.verify(req.headers.authorization.split(' ')[1], 'HoangHung', (err, decode) => {
            if (err) req.user = undefined;
            req.user = decode;
            next();
        });
    } else {
        req.user = undefined;
        next();
    }
});

const userRoute = require('./src/api/routes/user.route'),
    permissionRoute = require('./src/api/routes/permission.route'),
    productRoute = require('./src/api/routes/product.route');

userRoute(app);
permissionRoute(app);
productRoute(app);

app.listen(port);
console.log(`User started on: ${port}`)

